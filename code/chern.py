import numpy as np
import scipy.linalg as slinalg
import matplotlib.pyplot as plt
import pandas as pd


def build_U(vec1, vec2):
    in_product = np.dot(vec1.conj(), vec2)
    return in_product / np.abs(in_product)


def latF(H, k_vec, Dk, dim):
    """Calulating lattice field using the definition:
    F12 = ln[ U1 * U2(k+1) * U1(k_2)^-1 * U2(k)^-1 ]
    so for each k=(kx,ky) point, four U must be calculate.
    The lattice field has the same dimension of number of
    energy bands.

    in: k-point k_vec=(kx,ky), Dk=(Dkx,Dky), dim: dim of H(k)
    out: lattice field corresponding to each band as a n
    dimensional vec

    Function adapted from: https://github.com/khroushan/Chern/blob/master/chern_2DHall.py
    """

    # Here we calculate the band structure and sort
    # them from low to high eigenenergies
    k = k_vec
    E, aux_left, aux_right = slinalg.eig(H(k), left=True)
    idx = E.real.argsort()
    E_sort = E[idx].real
    psi_left = aux_left[:, idx]
    psi_right = aux_right[:, idx]

    k = np.array([k_vec[0] + Dk[0], k_vec[1]], float)
    E, aux_left, aux_right = slinalg.eig(H(k), left=True)
    idx = E.real.argsort()
    E_sort = E[idx].real
    psiDx_left = aux_left[:, idx]
    psiDx_right = aux_right[:, idx]

    k = np.array([k_vec[0], k_vec[1] + Dk[1]], float)
    E, aux_left, aux_right = slinalg.eig(H(k), left=True)
    idx = E.real.argsort()
    E_sort = E[idx].real
    psiDy_left = aux_left[:, idx]
    psiDy_right = aux_right[:, idx]

    k = np.array([k_vec[0] + Dk[0], k_vec[1] + Dk[1]], float)
    E, aux_left, aux_right = slinalg.eig(H(k), left=True)
    idx = E.real.argsort()
    E_sort = E[idx].real
    psiDxDy_left = aux_left[:, idx]
    psiDxDy_right = aux_right[:, idx]

    U1x = np.zeros((dim), dtype=complex)
    U2y = np.zeros((dim), dtype=complex)
    U1y = np.zeros((dim), dtype=complex)
    U2x = np.zeros((dim), dtype=complex)

    for i in range(dim):
        U1x[i] = build_U(psi_right[:, i], psiDx_right[:, i])
        U2y[i] = build_U(psi_right[:, i], psiDy_right[:, i])
        U1y[i] = build_U(psiDy_right[:, i], psiDxDy_right[:, i])
        U2x[i] = build_U(psiDx_right[:, i], psiDxDy_right[:, i])

    F12 = np.zeros((dim), dtype=complex)
    F12 = np.log(U1x * U2x * 1.0 / U1y * 1.0 / U2y)
    return F12, E_sort


def lattice_field_k(kx, ky, H, Dkx, Dky, dim):
    F12, _ = latF(H, np.array([kx, ky]), np.array([Dkx, Dky]), dim)
    return np.imag(F12[1])


def chern(H, BZ, Dk, dim, no_bands):
    integrated_F12 = np.zeros(dim, dtype=complex)
    for k in BZ:
        F12, _ = latF(H, k, Dk, dim)
        integrated_F12 += F12
    C = 0
    for n in no_bands:
        C += integrated_F12[n]
    return C / 2 / np.pi / 1j


if __name__ == "__main__":
    pass
