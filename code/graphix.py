import numpy as np
import matplotlib.pyplot as plt
import chern
import pandas as pd
from pylatexenc.latex2text import LatexNodes2Text

import tex

tex.useTex()

def gen_ticks(a,b,c,d):
    step = (d-c)/(b-a)
    lab = ["$"+str(i)+"$" for i in range(int(a),int(b)+1)]
    return [i*step for i in range(len(lab))],lab

def plot_weight(grid, weights, n_weight,name, x_title="", y_title=""):
    weight_map = []
    for w in weights:
        wm = 0
        for n in n_weight:
            wm += w[n,0]
        weight_map.append(wm)

    x,y = grid[:,0],grid[:,1]
    df = pd.DataFrame.from_dict(np.array([x,y,weight_map]).T)
    df.columns = ['X_value','Y_value','Z_value']
    df['Z_value'] = pd.to_numeric(df['Z_value'])
    pivotted= df.pivot('Y_value','X_value','Z_value')
    plt.imshow(pivotted,cmap='inferno')
    posx,labx = gen_ticks(np.min(x),np.max(x),0,int(np.sqrt(len(x))))
    posy,laby = gen_ticks(np.min(y),np.max(y),0,int(np.sqrt(len(y))))
    plt.xticks(posx,labx,fontsize=14)
    plt.yticks(posy,laby[::-1],fontsize=14)
    plt.title(name,fontsize=14)
    plt.xlabel(x_title,fontsize=14)
    plt.ylabel(y_title,fontsize=14)
    clb = plt.colorbar()
    clb.ax.set_title(r"$W_A^n$",fontsize=14)
    clb.ax.tick_params(labelsize=14)
    plt.savefig(f"../figs/{LatexNodes2Text().latex_to_text(name)}.pdf", format="pdf",bbox_inches="tight")
    plt.show()
    
    
def plot_lattice_field(H, BZ, Dk, dim, name,x_title="",y_title=""):
    X, Y = [k[0] for k in BZ], [k[1] for k in BZ]
    Z = np.vectorize(chern.lattice_field_k)(X, Y, H, Dk[0], Dk[1], dim)

    df = pd.DataFrame.from_dict(np.array([X,Y,Z]).T)
    df.columns = ['X_value','Y_value','Z_value']
    df['Z_value'] = pd.to_numeric(df['Z_value'])
    pivotted= df.pivot('Y_value','X_value','Z_value')
    plt.imshow(pivotted,cmap='viridis')
    posx,labx = gen_ticks(np.min(X),np.max(X),0,int(np.sqrt(len(X))))
    plt.xticks(posx,labx,fontsize=14)
    posy,laby = gen_ticks(np.min(Y),np.max(Y),0,int(np.sqrt(len(Y))))
    plt.yticks(posy[::-1],laby[::-1],fontsize=14)
    plt.title(name,fontsize=14)
    clb = plt.colorbar()
    clb.ax.set_title(r"$\bf{\Omega}(\bf{k})$",fontsize=14)
    clb.ax.tick_params(labelsize=14)
    plt.xlabel(x_title,fontsize=14)
    plt.ylabel(y_title,fontsize=14)
    plt.savefig(f"../figs/{LatexNodes2Text().latex_to_text(name)}.pdf", format="pdf",bbox_inches="tight")
    plt.show()


def plot_bands(
        path,
        bands,
        title="",
        pos_xticks=[],
        name_xticks=[],
        colors="blue",
        dim_fig=0,
        size=0.1,
):
    """Simple band diagram."""
    res_bands = len(path)
    nb_of_bands = len(bands[0])
    e_max = np.max(bands)
    e_min = np.min(bands)
    # quasimomentum = np.linspace(0, res_bands, res_bands)
    quasimomentum = path

    if not (isinstance(colors, str)):
        colors, color_min, color_max = colors

    for i in range(nb_of_bands):
        if isinstance(colors, str):
            plt.scatter(quasimomentum, [b[i] for b in bands], color=colors, s=size)
        else:
            plt.scatter(
                quasimomentum,
                [b[i] for b in bands],
                c=[c[i] for c in colors],
                cmap="viridis",
                norm=matplotlib.colors.Normalize(vmin=color_min, vmax=color_max),
                s=size,
            )

    if not (isinstance(colors, str)):
        cbar = plt.colorbar()
        cbar.ax.tick_params(labelsize=20)

    plt.xticks(pos_xticks, name_xticks, fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.ylabel(r"$E[\bf{k}]$", fontsize=20)
    plt.xlabel(r"$\textrm{Quasimomentum}\;[\bf{k}]$", fontsize=20)
    plt.title(r"$\textrm{" + title + "}$", fontsize=20)
    if dim_fig == 0:
        plt.axis((0, np.max(quasimomentum), e_min * 1.1, e_max * 1.1))
    else:
        plt.axis(dim_fig)
    plt.savefig(
        f"../figs/{LatexNodes2Text().latex_to_text(title)}.pdf",
        format="pdf",
        bbox_inches="tight",
    )
    plt.show()    

if __name__ == "__main__":
    pass
