"""
The `lattice_r` module provides functions for generating and manipulating
different types of lattices in real space.
"""

import random
import numpy as np
import matplotlib.pyplot as plt
from shapely.geometry.polygon import Polygon
from shapely.geometry import Point


def generate_hex_grid(n: int, a: float) -> np.ndarray:
    """n is the number of sites wanted, a is the step of the network
    we separate A and B sublattices for further use."""
    N = int(np.sqrt(n / 8))
    b = a * np.sqrt(3)
    c = 3 / 2 * a

    asub = [
        ((i + 1 / 4 * (1 + (-1) ** (j + 1))) * b, j * c)
        for i in range(-N, N)
        for j in range(-N, N)
    ]
    bsub = [(x, y + a) for x, y in asub]
    return np.array(asub + bsub)


def generate_hex_grid_lb_center(n: int, a: float) -> np.ndarray:
    """same but low left is at 0,0"""
    N = int(np.sqrt(n / 2))
    b = a * np.sqrt(3)
    c = 3 / 2 * a

    asub = [
        ((i + 1 / 4 * (1 + (-1) ** (j + 1))) * b, j * c)
        for i in range(0, N)
        for j in range(0, N)
    ]
    bsub = [(x, y + a) for x, y in asub]
    return np.array(asub + bsub)


def display_lattice(grid: np.ndarray, color: str = "blue") -> None:
    """
    Display a lattice of points in a scatter plot
    """
    x1, y1 = grid.T
    plt.scatter(x1, y1, color=color)


def rot(center, point, angle):
    """
    Rotate point around center by angle (in radians)
    """
    cx, cy = center
    px, py = point
    cos_theta = np.cos(angle)
    sin_theta = np.sin(angle)
    qx = cos_theta * (px - cx) - sin_theta * (py - cy) + cx
    qy = sin_theta * (px - cx) + cos_theta * (py - cy) + cy
    return (qx, qy)


def generate_hex_hex_stretch(
    n: int = 42, a: float = 1, s: float = 1, φ: float = 0
) -> np.ndarray:
    """
    Generates a honeycomb lattice with hexagonal shape.
    The function first generates a triangular lattice with n points and lattice
    parameter 3a. Then it shifts the lattice center to the origin and selects the
    subset of points that are inside a hexagon whose side length is twice the minimum
    absolute coordinate distance. Finally, it maps the points onto a hexagonal grid
    using the `map_hex_on_grid` function, and concatenates the resulting a- and b-
    sublattices into a single array.

    Args:
        n (int): ~ Number of points in the lattice.
        a (float): Distance between nearest neighbors.
        s (float): Stretching factor applied to the lattice.
        φ (float): Random angle of rotation applied to the lattice, in radians.

    Returns:
        numpy.ndarray: An (n, 2) array containing the coordinates of the lattice points.
    """
    n = int(n / 2) + int(n / 2) % 2
    points = generate_triangular_lattice(n, 3 * a)
    x, y = points.T
    x, y = x - max(x) / 2, y - max(y) / 2
    points = np.column_stack((x, y))
    grid = np.array(
        select_points_in_hexagon(
            points, (min(abs(x)), min(abs(y))), int(np.sqrt(n)) + 0.1
        )
    )
    asub, bsub = map_hex_on_grid(grid, a, s, φ)
    return np.array(asub + bsub)


def select_points_in_hexagon(points, center, side_length):
    """Selects points in a given hexagon"""
    hexagon = Polygon(
        [
            (
                center[0] + side_length * np.cos(angle),
                center[1] + side_length * np.sin(angle),
            )
            for angle in np.linspace(0, 2 * np.pi, 7)[:-1]
        ]
    )

    selected_points = []
    for point in points:
        if Point(point).within(hexagon):
            selected_points.append(point)

    return selected_points


def hex_coords(x: float = 0, y: float = 0, a: float = 1):
    """Generates coords of an hexagon of size a centered in (x,y)"""
    coords = []
    for i in range(6):
        angle_rad = np.pi / 3 * i
        coord_x = x + a * np.cos(angle_rad)
        coord_y = y + a * np.sin(angle_rad)
        coords.append((coord_x, coord_y))
    return coords


def generate_triangular_lattice(N: int, a: float) -> np.ndarray:
    """Generates a triangular lattice of points with a specified size.

    Args:
        N: An even integer representing the number of points in each direction.
        a: A float representing the distance between neighboring points.

    Returns:
        A numpy array of shape (N^2, 2) representing the coordinates of the lattice points.
    """
    if N % 2 != 0:
        raise ValueError("N must be even.")
    b = a * np.sqrt(3) / 2
    x, y = np.meshgrid(np.arange(N), np.arange(N), indexing="ij")
    x = x.reshape((N * N,)).astype("float")
    y = y.reshape((N * N,)).astype("float")
    x[::2] += 0.5
    coords = np.column_stack((x * a, y * b))
    return coords


def map_hex_on_grid(grid, a, s, φ, border="none"):
    """Maps hexagonal coordinates onto a grid and adds borders to the resulting shape.

    Args:
        grid (list): A list of tuples representing the x and y
    coordinates of the hexagons.
        a (float): The size of the hexagon.
        s (float): The stretch factor of the hexagon.
        φ (float): The angle of rotation for the hexagon.
        border (str): The type of border to add to the shape.
    Can be "none" for no border, "armchair" for armchair border.

    Returns:
        tuple: A tuple containing two lists representing the x and y
    coordinates of the sublattices asub and bsub.

    Raises:
        ValueError: If an invalid border type is provided.
    """
    asub, bsub = [], []
    xs, ys = np.array(grid).T
    xs = sorted(list(set(xs)))
    ys = sorted(list(set(ys)))
    penul_xs, snd_xs = xs[-2], xs[1]
    for x, y in grid:
        θ = φ * (random.random() - 0.5) * 2
        hexc = list(map(lambda e: rot((x, y), e, θ), hex_coords(x, y, s)))
        asub += hexc[::2]
        bsub += hexc[1::2]
        if border == "armchair":
            if x == penul_xs:
                θ = φ * (random.random() - 0.5) * 2
                hexc = list(
                    map(lambda e: rot((x, y), e, θ), hex_coords(x + 3 * a, y, s))
                )
                asub.append(hexc[2])
                bsub.append(hexc[3])
                asub.append(hexc[4])
            elif x == snd_xs:
                θ = φ * (random.random() - 0.5) * 2
                hexc = list(
                    map(lambda e: rot((x, y), e, θ), hex_coords(x - 3 * a, y, s))
                )
                asub.append(hexc[0])
                bsub.append(hexc[1])
                bsub.append(hexc[5])
        else:
            if border == "none":
                pass
            else:
                raise ValueError

    return asub, bsub


def generate_hex_square_stretch(n=100, a=1, s=1, φ=0):
    """
    Generate a hexagonal lattice with 'n' sites, where the lattice constant of the
    underlying triangular lattice is '3 * a'.
    The resulting lattice is returned as a numpy array with alternating sublattices a
    and b. This ordering allows for easy differentiation between the two sublattices.

    Args:
        n (int): ~ The number of points in the lattice.
        a (float): The lattice constant.
        s (float): The stretching factor.
        φ (float): The rotation angle in radians, each hexagons will be randomly
    rotated of a value in (-φ ,φ )

    Returns:
        lattice (nd.array): a numpy array representing the x and y
    coordinates of the lattice points.
    """
    N = int(np.sqrt(n / 6))
    triang = generate_triangular_lattice(N, 3 * a)
    asub, bsub = map_hex_on_grid(triang, a, s, φ, "armchair")
    return np.array(asub + bsub)


def generate_hex_grid_stretch(N_cluster_side=4, a=1):
    triang = generate_triangular_lattice(N_cluster_side, 3 * a)
    grid = []
    for point in triang:
        hexc = hex_coords(point[0], point[1], a)
        grid += hexc
    return np.array(grid)


if __name__ == "__main__":
    g = generate_hex_square_stretch(n=100, a=1, s=1, φ=0)
    display_lattice_with_edges(g)

    g = generate_hex_grid(n=100, a=1)
    display_lattice(g[: len(g) // 2], "blue")
    display_lattice(g[len(g) // 2 :], "red")
    plt.show()

    g = generate_hex_grid_lb_center(n=100, a=1)
    display_lattice(g[: len(g) // 2], "blue")
    display_lattice(g[len(g) // 2 :], "red")
    plt.show()

    g = generate_hex_hex_stretch(n=42, a=1, s=1, φ=0)
    display_lattice(g[: len(g) // 2], "blue")
    display_lattice(g[len(g) // 2 :], "red")
    plt.show()

    g = generate_hex_square_stretch(n=100, a=1, s=1, φ=0)
    display_lattice(g[: len(g) // 2], "blue")
    display_lattice(g[len(g) // 2 :], "red")
    plt.show()

    g = generate_triangular_lattice(N=20, a=3)
    display_lattice(g)
    plt.show()

    g = generate_hex_square_stretch(n=100, a=1, s=1, φ=0)
    display_lattice(g)
    plt.savefig("hex_square_s1.pdf", format="pdf", bbox_inches="tight", dpi=600)

    plt.show()
