import numpy as np
import matplotlib.pyplot as plt
from functools import partial

import lattice_f as lf
import plot_3d as p3d
import bz
import chern as ch
import spin_chern as sc

a = 1
e_vect = [(-a*np.sqrt(3)/2,-3/2*a),(a*np.sqrt(3),0),(-a*np.sqrt(3)/2,3/2*a)]
a_vect = [(0,a),(-np.sqrt(3)/2*a,-a/2),(np.sqrt(3)/2*a,-a/2)]

def H(k, t, λ_SO):
    M = np.zeros((2,2),dtype=complex)                       
    for γ in range(3):
        ke = np.dot(k,e_vect[γ])
        ka = np.dot(k,a_vect[γ])
        M += np.array([[2*np.real(λ_SO)*np.cos(ke)-2*np.imag(λ_SO)*np.sin(ke),np.exp(1j*ka)],
                       [np.exp(-1j*ka),-2*np.real(λ_SO)*np.cos(ke)+2*np.imag(λ_SO)*np.sin(ke)]])
    return M
 

def H_prime(k, t, λ_SO, λ_R, λ_v):
    Hk = H(k, t, λ_SO)
    Hk2 = H(k, t, np.conj(λ_SO))
    Mk = np.zeros((2,2),dtype=complex)

    for γ in range(3):
        ke = np.dot(k,e_vect[γ])
        ka = np.dot(k,a_vect[γ])
        c = γ 
        if γ == 2:
            c = -1
        Mk += np.array([[0,np.exp(1j*2*np.pi*c/3+1j*ka)],
                       [-np.exp(1j*2*np.pi*c/3-1j*ka),0]])
    # S = np.exp(1j*2*a*k[1]) + np.exp(1j*a*k[1])*np.cos(np.sqrt(2)*a)*2
    # Mk = np.array([[0,S],
    #                [-np.conj(S),0]])
    Mk = Mk*λ_R/2

    I_v = np.diag([-λ_v, λ_v, -λ_v, λ_v])

    return np.block([[Hk,Mk],
                     [np.conj(Mk.T),Hk2]]) + I_v

def ham(k, t, λ_SO, λ_R, λ_v):
    # t = 1
    # λ_SO = 0.06j
    # λ_R = 0.3
    # λ_v = 0.1

    # t = 1
    # λ_SO = 0.3j
    # λ_R = 0.25
    # λ_v = 2.5
    
    return H_prime(k, t, λ_SO, λ_R, λ_v)



def get_ham_m(k, t, λ_SO, λ_R, λ_v):
    Hp = H_prime(k, t, λ_SO, λ_R, λ_v)
    psp_m, _ = sc.psp_from_H(Hp)
    return psp_m


def get_ham_p(k, t, λ_SO, λ_R, λ_v):
    Hp = H_prime(k, t, λ_SO, λ_R, λ_v)
    _, psp_p = sc.psp_from_H(Hp)
    return psp_p

def spin_chern_km(t, λ_SO,λ_R, λ_v, BZ , Dk, dim, no_bands):
    Cm = ch.chern(partial(get_ham_m, t=t, λ_SO=λ_SO, λ_R=λ_R, λ_v=λ_v), BZ , Dk, dim, [0])
    Cp = ch.chern(partial(get_ham_p, t=t, λ_SO=λ_SO, λ_R=λ_R, λ_v=λ_v), BZ , Dk, dim, [0])
    return (Cp-Cm)/2
    
    
    

step = 0.1
BZ, _, Dk = lf.generate_bz(a, step)
BZ = BZ + (1e-4, 1e-4)
dim = 4

t, λ_SO, λ_R, λ_v = 1, (1/3/np.sqrt(3)+0.1)*1j, 0, 1

λ_SOs = [0.01 + i/50 for i in range(50)]
λ_vs = [0.01+i/10 for i in range(50)]

for λ_SO in λ_SOs:
    for λ_v in λ_vs:
        C = spin_chern_km(t, λ_SO*1j,λ_R, λ_v, BZ , Dk, dim, [0])
        print(λ_SO,",", λ_v,",", C)

exit()
        


n_dots = 300

path = lf.generate_path_bz_2(a, ["Γ", "K", "M", "K'", "Γ"], n_dots)
# path,_,_ = lf.generate_bz(a, 0.1)
# path = bz.square_bz(4,45)
# path = lf.generate_path_bz_2(a, ["Γ", "K", "M", "Γ"], n_dots)


energy = []
for i, k in enumerate(path):

    h = ham(k)

    E = np.linalg.eigvals(h)
    E.sort()
    tmp = []
    for e in E:
        tmp.append(np.real(e))
        plt.scatter(i, np.real(e), color="blue", s = 5)
    energy.append([np.real(E[0]),np.real(E[-1])])
    energy.append(tmp)


plt.xlabel("",fontsize=20)
plt.ylabel("",fontsize=20)
plt.title(r"$\Gamma\rightarrow K \rightarrow K' \rightarrow \Gamma$",fontsize=20)
plt.savefig("../figs/kanemele_fig_2_e.pdf",format="pdf",bbox_inches='tight')
plt.show()
# p3d.plot_band_structure_3d(energy,path,"KM prodan")
