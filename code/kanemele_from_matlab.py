import numpy as np
import matplotlib.pyplot as plt
import lattice_f as lf

# Paramètres initiaux
parameters = [1, 1, 0.3, 0.25]  # Remplacez par vos paramètres réels
t, V, LSO, LR = parameters
a1 = 0.5 * np.array([-np.sqrt(3), 3])
a2 = 0.5 * np.array([np.sqrt(3), 3])
a = np.linalg.norm(1) / np.sqrt(3)  # Remplacez a1 par votre vecteur réel

# Matrices Pauli
sig_x = np.array([[0, 1], [1, 0]])
sig_y = np.array([[0, -1j], [1j, 0]])
sig_z = np.array([[1, 0], [0, -1]])
I = np.array([[1, 0], [0, 1]])

# Opérateur de renversement temporel
T = 1j * np.kron(sig_y, I)

# Gamma matrices
G1 = np.kron(sig_x, I)
G2 = np.kron(sig_z, I)
G3 = np.kron(sig_y, sig_x)
G4 = np.kron(sig_y, sig_y)
G5 = np.kron(sig_y, sig_z)
G15 = 1 / (2j) * (G1 @ G5 - G5 @ G1)
G23 = 1 / (2j) * (G2 @ G3 - G3 @ G2)
G24 = 1 / (2j) * (G2 @ G4 - G4 @ G2)
G34 = 1 / (2j) * (G3 @ G4 - G4 @ G3)
G35 = 1 / (2j) * (G3 @ G5 - G5 @ G3)
G45 = 1 / (2j) * (G4 @ G5 - G5 @ G4)

# Initialisation des tableaux
k_path = np.linspace(0, 2 * np.pi, num=100)  # Remplacez par votre chemin k réel
E1 = np.zeros(len(k_path))
E2 = np.zeros(len(k_path))
E3 = np.zeros(len(k_path))
E4 = np.zeros(len(k_path))
TRIM = np.zeros(len(k_path))
TRIM2 = np.zeros(len(k_path))
valence = 0
cond = 0


def ham(k):

    k1 = np.dot(k, a1)
    k2 = np.dot(k, a2)

    # Calcul de H
    d45 = t * (1 + np.cos(k1) + np.cos(k2))
    d35 = -t * (np.sin(k1) + np.sin(k2))
    d15 = 2 * LSO * (np.sin(k1) - np.sin(k2) - np.sin(k1 - k2))
    d34 = V

    print(G45.shape)
    print(G35.shape)
    print(G15.shape)
    print(G34.shape)
    print(d45)
    print(d35)
    print(d15)
    print(d34)
    print(k)
    print(k1)
    print(k2)
    H = d45 * G45 + d35 * G35 + d15 * G15 + d34 * G34

    # Calcul de H_R
    x = (k1 + k2) / 2
    y = (k2 - k1) / 2
    d3 = np.sqrt(3) * LR * np.sin(y) * np.cos(x)
    d4 = -np.sqrt(3) * LR * np.sin(x) * np.sin(y)
    d23 = -LR * np.sin(x) * np.cos(y)
    d24 = LR * (1 - np.cos(x) * np.cos(y))

    H_R = d3 * G3 + d4 * G4 + d23 * G23 + d24 * G24
    H = H + H_R + H_R.T
    return H


# Génération de la matrice de Hamilton et diagonalisation


n_dots = 200
a = 1
path = lf.generate_path_bz_2(a, ["Γ", "K", "M", "K'", "Γ"], n_dots)
# path = lf.generate_path_bz_2(a, ["Γ", "K'", "Γ", "K'", "Γ"], n_dots) 


for i, k in enumerate(path):

    H = ham(k)

    E = np.linalg.eigvals(H)
    for j in range(len(E)):
        plt.scatter(i, E[j], color="blue",s=5)

plt.show()
