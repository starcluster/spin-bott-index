import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import eig,eigh
import os
import sys

sys.path.append('../../chapter-2-thesis/code')
import bott
import haldane_real as hr
import lattice_r as lr

plt.rc("text.latex", preamble=r"\usepackage{amsmath}")

plt.rcParams.update(
    {"text.usetex": True, "font.family": "sans-serif", "font.sans-serif": ["Helvetica"]}
)


def get_sigma_bott(N):
    """N is the number of cluster"""
    return np.kron(np.array([[1,0],[0,-1]]), np.eye(N//2))


def make_projector(vl, vr):
    """Create a projector from left and right eigenvectors"""
    n = vl[0].shape[0]
    P = np.zeros((n, n), dtype="complex128")
    for i in range(len(vl)): # s'arrêter dans le gap
        vi = vl[i]
        vit = vr[i]
        P += np.outer(vi, vit.conj())/np.dot(vit.conj(), vi)

    # w, v = np.linalg.eig(P)
    # print(w)
    # exit()
    return P


def get_p_sigma_p_bott(w, vl, vr, sigma, omega):
    i = np.searchsorted(w, omega)
    print(f"{omega=}")
    print(f"{i=}")
    P = make_projector(vl[0:i], vr[0:i])

    # P = make_projector(vl, vr)

    return P @ sigma @ P


def sort_vectors(w, vl, vr):
    idx = w.argsort()
    vl = vl[:, idx]
    vr = vr[:, idx]
    w = w[idx]
    return w, vl, vr

def sort_vectors_and_listify(w, vl, vr):
    idx = w.argsort()
    vl = vl[:, idx]
    vr = vr[:, idx]
    vl = [vl[:,i] for i in range(vl.shape[0])]
    vr = [vr[:,i] for i in range(vr.shape[0])]
    w = w[idx]
    return w, vl, vr


def contient_doublons_avec_precision(lst, precision):
    """
    Vérifie si une liste contient plusieurs fois le même réel avec une précision arbitraire.

    :param lst: La liste à vérifier.
    :param precision: La précision à utiliser pour la comparaison.
    :return: True si la liste contient des doublons avec la précision spécifiée, False sinon.
    """
    for i, x in enumerate(lst):
        for j, y in enumerate(lst):
            if i != j and np.allclose(x, y, atol=precision):
                return True
    return False

def spin_bott(grid, vl, vr, w, threshold_psp, threshold_energy, N_cluster):
    # plt.imshow(np.abs(vr))
    # plt.colorbar()
    # plt.savefig("vect_sb_2.png",format="png",bbox_inches='tight')
    # plt.show()    
    Σ = get_sigma_bott(N_cluster)
    psp = get_p_sigma_p_bott(w, vl, vr, Σ, threshold_psp)
    w, vl, vr = eig(psp, left=True, right=True)
    w_psp, vl_psp, vr_psp = sort_vectors(*eig(psp, left=True, right=True))

    # x = np.linspace(0, w_psp.shape[0], w_psp.shape[0])
    # plt.scatter(x, np.array(w_psp).real, color="black")
    # plt.xlabel(r"$\textrm{Eigenvalues}$ $\lambda$--$P\sigma P$", fontsize=20)
    # plt.ylabel(r"$\textrm{DOS}$", fontsize=20)
    # plt.title(f"$t_2={t2}$",fontsize=20)
    # plt.savefig(f"SPECTRUM_psp_{t1=}_{t2=}.pdf",format="pdf",bbox_inches='tight')
    # plt.show()
    idx = w_psp.argsort()
    # print(vr_psp)

    # n = vr_psp[0].shape[0]
    # vr_psp_inf = []
    # vr_psp_sup = []
    # for evr in [vr_psp[:,i] for i in range(n)]:
    #     demi_vect_sum = np.sum(np.abs(evr[0:n//2]))
    #     if demi_vect_sum < 1e-6:
    #         vr_psp_inf.append(evr.T)
    #     else:
    #         vr_psp_sup.append(evr.T)
    #         # print(np.real(e))
    #         # print(ev)
    #         # print(demi_vect_sum)
    #         # abc= input()
    # print(np.array(vr_psp_inf).shape)
    # print(np.array(vr_psp_sup).shape)
    # print(n)
    # vr_psp = np.block([[np.array(vr_psp_inf),np.zeros_like(vr_psp_inf)],
    #                    [np.zeros_like(vr_psp_sup),np.array(vr_psp_sup)]])
    # vr_psp = vr_psp[:,n//2:n//2*3]
    # vl = vect_of_h1_l + vect_of_h2_l
    # vr = vect_of_h1_r + vect_of_h2_r

    # fig, axs = plt.subplots(2, 2)
    # axs[0, 0].plot(x, y1) # TL
    # axs[0, 0].set_title("Figure 1")
    # axs[0, 1].plot(x, y2) # TR
    # axs[0, 1].set_title("Figure 2")

    # plt.tight_layout()
    # plt.show()
    # for i in range(vr_psp.shape[0]):
    #     vr_psp[100,i] = w_psp[i]    
    # plt.imshow(np.abs(vr_psp))
    # plt.colorbar()

    # plt.axvline(x=vr_psp.shape[0]//6)
    # plt.savefig("vect_sb.png",format="png",bbox_inches='tight')

    # plt.show()

    bm = bott.bott(
        grid, vr_psp, w_psp, threshold_energy, pol=False, dagger=False, verbose=False
    )
    # np.roll(np.arange(vr_psp.shape[1]), shift = -1)
    idx = (-w_psp).argsort()
    print(idx)
    bp = bott.bott(
        grid,
        vr_psp[:,idx],
        -w_psp[idx],
        threshold_energy,
        pol=False,
        dagger=False,
        verbose=False,
    )
    print(bp,bm)
    return (bp - bm) / 2



if __name__ == "__main__":
    a = 1
    N_cluster_side = 6
    N_cluster = N_cluster_side**2
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side,a=a)

    t1 = 1

    Ms = np.linspace(0,10,10)
    t2s = np.linspace(0,2,11)
    t2 =  (1/3/np.sqrt(3)-0.1)* 1j
    # t2 = (t2s[10])*1j
    t2 = 2j
    M = Ms[2]
    M = 6
    print(t2)
    print(M)
    # M = 1
    H_haldane_1 = hr.create_hamiltonian_haldane_pbc(lattice, a, t1, t2, M)

    # w, vl, vr = sort_vectors(*eig(H_haldane_1, left=True, right=True))
    
    threshold_energy = -0.1

    # plt.imshow(np.abs(vr))
    # plt.colorbar()
    # plt.savefig("vect_b.png",format="png",bbox_inches='tight')
    # plt.show()
    # b1 = bott.bott(
    #     lattice, vr, w, threshold_energy, pol=False, dagger=False, verbose=False,vl=vl
    # )

    # print(f"{b1=}")

    
    H_haldane_2 = hr.create_hamiltonian_haldane_pbc(lattice, a, t1, -t2, M)
    n = H_haldane_1.shape[0]
    H_km = np.block([[H_haldane_1, np.zeros((n,n))],
                     [np.zeros((n,n)), H_haldane_2]])

    # w, vl, vr = sort_vectors(*eig(H_haldane_2, left=True, right=True))
    
    # threshold_energy = -0.1
    
    # b1 = bott.bott(
    #     lattice, vl, w, threshold_energy, pol=False, dagger=False, verbose=False,vl=vl
    # )
    # print(f"{b1=}")
    # print(H_km.shape)


    lattice = np.concatenate((lattice, lattice))
    eigenvalues, eigenvectors_l, eigenvectors_r = eig(H_km,left=True, right=True)

    print("????????????")
    print(H_km.shape)
    print(H_km)
    print(np.sum(H_km))
    print(np.sum(np.abs(H_km - np.conj(H_km.T))))
    print(eigenvectors_l[0,0])
    print(eigenvectors_r[0,0])
    print("????????????")
    

    eigenvalues, eigenvectors_r = eigh(H_km)
    print(np.sum(np.abs(H_km - np.conj(H_km.T))))
    print(eigenvectors_r[0,0])


    els, vl, vr = sort_vectors(eigenvalues, eigenvectors_l, eigenvectors_r)
    vr = eigenvectors_r
    
    els, vl, vr = sort_vectors_and_listify(eigenvalues, eigenvectors_l, eigenvectors_r)
    # print(contient_doublons_avec_precision(els, 1e-6))
    # print(els)
    # exit()
    # print(M,",",bott.bott(lattice=lattice, eigvec=evs, frequencies=els, omega=0, pol=False, dagger=False, verbose=False))
    # print(vr[:,2])
    # exit()

    threshold_psp = 0
    threshold_energy = -0.1
    eigenvectors_r = [eigenvectors_r[:,i] for i in range(eigenvectors_r.shape[0])]
    vect_of_h1_r = []
    vect_of_h2_r = []
    vect_of_h1_l = []
    vect_of_h2_l = []

    n = eigenvectors_r[0].shape[0]
    for e,evr,evl in zip(eigenvalues,vr,vl):
        demi_vect_sum = np.sum(np.abs(evr[0:n//2]))
        if np.real(e) < 0:
            if demi_vect_sum < 1e-6:
                vect_of_h2_r.append(evr)
                vect_of_h2_l.append(evl)
            else:
                vect_of_h1_r.append(evr)
                vect_of_h1_l.append(evl)
            # print(np.real(e))
            # print(ev)
            # print(demi_vect_sum)
            # abc= input()
    # vl = vect_of_h1_l + vect_of_h2_l
    # vr = vect_of_h1_r + vect_of_h2_r

    
    # fig, ax = plt.subplots()
    # ax.imshow(np.real(vr[0]).reshape(6,))
    # fig2, ax2 = plt.subplots()
    # ax2.imshow(np.real(vl[0]).reshape(6,))
    # plt.show()

          
    sb = spin_bott(lattice, vr, vr, els, threshold_psp, threshold_energy, H_km.shape[0])
    
    print(f"{sb=}")


