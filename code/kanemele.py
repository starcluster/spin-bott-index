import numpy as np
import matplotlib.pyplot as plt
from numpy import cos, sin, exp, sqrt

import graphix
from modele import Modele
import lattice_f as lf
import lattice_r as lr
import chern
import bz
import plot_3d as p3d


class KaneMele(Modele):
    def __init__(self, t, λ_SO, λ_R, λ_v):
        super().__init__()
        self.t = t
        self.λ_SO = λ_SO
        self.λ_R = λ_R
        self.λ_v = λ_v

    def compute_ham(self, k):
        a = 1/np.sqrt(3)

        k_x, k_y = k[0], k[1]
        Γ_k = 2 * cos(a * k_x / 2) * exp(-1j * a * k_y / 2 / sqrt(3)) + exp(
            1j * a * k_y / sqrt(3)
        )
        M_k = 2 * sin(a * k_x) - 4 * sin(a * k_x / 2) * cos(a * k_y * sqrt(3) / 2)
        N_k = -2 * sqrt(3) * cos(a * k_x / 2) * sin(sqrt(3) * a * k_y / 2) - 1j * 2 * (
            sin(a * k_x / 2) * cos(sqrt(3) * a * k_y / 2) + sin(a * k_x)
        )
        ham_up = np.zeros((4, 4), dtype="complex")
        ham_diag = np.diag(
            [
                self.λ_SO * M_k - self.λ_v,
                -self.λ_SO * M_k + self.λ_v,
                -self.λ_SO * M_k - self.λ_v,
                self.λ_SO * M_k + self.λ_v,
            ]
        )
        ham_up[0, 1] = -self.λ_R * N_k
        ham_up[0, 2] = -self.t * Γ_k
        ham_up[1, 3] = -self.t * Γ_k
        ham_up[2, 3] = self.λ_R * N_k

        self.ham = ham_diag + ham_up + np.conj(ham_up.T)
        return self.ham

    def compute_gap(self, path):
        B2 = [b[1] for b in self.bands]
        B3 = [b[2] for b in self.bands]
        return min(B3) - max(B2)


class KaneMele2(Modele):
    def __init__(self, t0, t1, M, φ):
        super().__init__()
        self.t0 = t0
        self.t1 = t1
        self.M = M
        self.φ = φ
        self.a = 1

    def compute_ham(self, k):
        kx, ky = k[0], k[1]
        K = self.t0 * (
            np.exp(1j * ky * self.a / np.sqrt(3))
            + 2 * np.exp(-1j * ky * a / 2 / np.sqrt(3)) * np.cos(kx * a / 2)
        )

        def Q1(φ):
            return (
                2
                * self.t1
                * np.cos(φ)
                * (
                    np.cos(kx * self.a)
                    + 2 * np.cos(kx * self.a / 2) * cos(ky * np.sqrt(3) * self.a / 2)
                )
            )

        H0 = np.array([[self.M + Q1(self.φ), K], [np.conj(K), -self.M + Q1(φ)]])

        def Q2(φ):
            return (
                2
                * self.t1
                * np.sin(φ)
                * (
                    sin(kx * self.a)
                    - 2 * np.sin(kx * self.a / 2) * np.cos(ky * np.sqrt(3) * self.a / 2)
                )
            )

        H_SOC = np.array([[Q2(self.φ), 0], [0, Q2(-self.φ)]])
        σ_z = np.array([[1, 0], [0, -1]])
        self.ham = np.kron(np.eye(2), H0) + np.kron(σ_z, H_SOC)
        return self.ham

    def compute_gap(self, path):
        B2 = [b[1] for b in self.bands]
        B3 = [b[2] for b in self.bands]
        return min(B3) - max(B2)


def gen_band_structure(t, λ_SO, λ_R, λ_v, n_dots, title):
    path = lf.generate_path_bz_2(a/np.sqrt(3), ["Γ", "K", "M", "K'", "Γ"], n_dots)
    kmf = KaneMele(t, λ_SO, λ_R, λ_v)
    kmf.compute_bands(path)
    x_band = [i for i in range(path.shape[0])]
    graphix.plot_bands(
        x_band,
        kmf.bands,
        title=title,
        pos_xticks=[i * n_dots // 5 for i in range(5)],
        name_xticks=[r"$\Gamma$", r"$K$", r"$M$", r"$K'$", r"$\Gamma$"],
        colors="blue",
    )


def gen_band_structure_2(t0, t1, M, φ, n_dots, title):
    path = lf.generate_path_bz_2(a, ["Γ", "K", "M", "K'", "Γ"], n_dots)
    kmf = KaneMele2(t0, t1, M, φ)
    kmf.compute_bands(path)
    x_band = [i for i in range(path.shape[0])]
    graphix.plot_bands(
        x_band,
        kmf.bands,
        title=title,
        pos_xticks=[i * n_dots // 5 for i in range(5)],
        name_xticks=[r"$\Gamma$", r"$K$", r"$M$", r"$K'$", r"$\Gamma$"],
        colors="blue",
    )


def gen_chern_number(t, λ_SO, λ_R, λ_v, step, n_dots, dim, no_bands):
    BZ, border, Dk = lf.generate_bz(a, step)
    kmf = KaneMele(t, λ_SO, λ_R, λ_v)
    C = chern.chern(kmf.compute_ham, BZ, Dk, dim, no_bands)
    print(C)


def gen_berry_curvature(t, λ_SO, λ_R, λ_v, step, n_dots, dim, no_bands, title):
    BZ, border, Dk = lf.generate_bz(a, step)
    grid_berry_curvature = bz.square_bz(2 * np.pi, n_dots)
    kmf = KaneMele(t, λ_SO, λ_R, λ_v)
    lr.display_lattice(BZ)
    graphix.plot_lattice_field(
        kmf.compute_ham, grid_berry_curvature, Dk, dim, r"$\textrm{Berry Curvature KM}$"
    )


def gen_band_structure_3d(t, λ_SO, λ_R, λ_v, n_dots):
    grid = bz.square_bz(6, n_dots)
    kmf = KaneMele(t, λ_SO, λ_R, λ_v)
    kmf.compute_bands(grid)

    p3d.plot_band_structure_3d(kmf.bands, grid, "Haldane")


if __name__ == "__main__":
    # a = np.sqrt(3)
    a = 1
    n_dots = 45
    t = 1
    λ_SO = 0.3
    λ_R = 0.25
    λ_v = 1
    # t0 = -1
    # t1 = -0.3
    # M = 0.3
    # φ = 3

    # gen_band_structure_3d(t, λ_SO, λ_R, λ_v ,n_dots)
    # exit()

    gen_band_structure(t, λ_SO, λ_R, λ_v, n_dots, "KaneMele - yu -"+f"{t},{λ_SO},{λ_R},{λ_v}")
    gen_band_structure_3d(t, λ_SO, λ_R, λ_v, n_dots)
   
    exit()
    gen_band_structure_2(t0, t1, M, φ, n_dots, "KaneMele2")
    exit()

    step = 0.1
    dim = 4
    no_bands = [0, 1]
    title = f"$t = {t} - \lambda_S = {λ_SO} - \lambda_R = {λ_R} - \lambda_v = {λ_v}$"
    # gen_band_structure(t, λ_SO, λ_R, λ_v ,n_dots, f"\n" + title)
    gen_band_structure(t, λ_SO, λ_R, λ_v, n_dots, f"$truc$")
    gen_chern_number(t, λ_SO, λ_R, λ_v, step, n_dots, dim, no_bands)
    gen_berry_curvature(
        t,
        λ_SO,
        λ_R,
        λ_v,
        step,
        n_dots,
        dim,
        no_bands,
        r"$\textrm{Berry curvature}$\newline" + title,
    )
    # lr.display_lattice(path,color="red")
    # plt.show()
