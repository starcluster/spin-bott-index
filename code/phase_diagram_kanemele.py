import matplotlib.pyplot as plt
import numpy as np
import csv
import sys
from scipy.linalg import eig

import spin_bott as sb
sys.path.append('../../chapter-2-thesis/code')
import lattice_r as lr
import haldane_real as hr
import tex

tex.useTex()

def phase_diagram_kanemele(N_cluster_side, a, t1):
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side,a=a)
    lattice_x2 = np.concatenate((lattice, lattice))
    Ms = np.linspace(-9,9,40)
    t2s = np.linspace(-2.,2,40)
    x = []
    y = []
    bs = []
    for M in Ms:
        for t2 in t2s:
            H = minimal_H_km(lattice, a, t1, t2*1j, M)
            eigenvalues, eigenvectors_r = np.linalg.eigh(H)
            els, evl , evr = sb.sort_vectors_and_listify(eigenvalues, eigenvectors_r, eigenvectors_r)
            threshold_psp = -0.1
            threshold_energy = -0.1

            c_sb = sb.spin_bott(lattice_x2, evl, evr, els, threshold_psp, threshold_energy, H.shape[0])
            x.append(M)
            y.append(t2)
            bs.append(c_sb)
            print(f"{M=},{t2=},{c_sb=}")

    with open("../data/phase_diagram_kanemele_bott.csv", mode="w", newline="") as csvf:
        writer = csv.writer(csvf)

        for k,l,m in zip(x,y,bs):
            writer.writerow([k,l,m])

def plot_phase_diagram_kanemele():
    data = np.genfromtxt("../data/phase_diagram_kanemele_bott.csv", delimiter=',', dtype=float, encoding=None)
    plt.scatter(data[:,0],data[:,1],c=data[:,2])
    plt.plot([-100,100],[-100/3/np.sqrt(3),100/3/np.sqrt(3)],color="black" )
    plt.colorbar()
    plt.xlabel(r"$M$",fontsize=20)
    plt.ylabel(r"$t_2$",fontsize=20)
    plt.title(r"\textrm{Spin Bott Index} $N=432$",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.axis((-9,9,-2,2.1))
    plt.savefig("../figs/phase_diagram_kanemele.pdf",format="pdf",bbox_inches='tight')
    plt.show()
            
def minimal_H_km(lattice, a, t1, t2, M):
    H_haldane_1 = hr.create_hamiltonian_haldane_pbc(lattice, a, t1, t2, M)
    H_haldane_2 = hr.create_hamiltonian_haldane_pbc(lattice, a, t1, -t2, M)
    n = H_haldane_1.shape[0]
    H_km = np.block([[H_haldane_1, np.zeros((n,n))],
                     [np.zeros((n,n)), H_haldane_2]])
    return H_km
    
if __name__ == "__main__":
    N_cluster_side = 4
    a = 1
    t1 = 1
    # phase_diagram_kanemele(N_cluster_side=N_cluster_side, a=a, t1=t1)
    plot_phase_diagram_kanemele()
