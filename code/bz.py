import numpy as np


def square_bz(l, n_dots):
    kx = np.linspace(0, l, n_dots)
    ky = np.linspace(0, l, n_dots)
    return np.column_stack([np.repeat(kx, n_dots), np.tile(ky, n_dots)])
