import matplotlib.pyplot as plt
import numpy as np
from scipy.linalg import eig

import spin_bott as sb

if __name__ == "__main__":
    M = np.ones((6,6)) + 0.1*np.eye(6)
    e, evl, evr = eig(M, left=True, right=True)

    # evl_l = [evl[:,i] for i in range(6)]
    # evr_l = [evr[:,i] for i in range(6)]
    # psp = sb.get_p_sigma_p_bott(e, evl_l, evr_l, np.eye(6), np.max(e)+1)
    # e_proj, evl_proj, evr_proj = eig(psp, left=True, right=True)
    # plt.imshow(np.abs(evr_psp))
    # plt.show()
    proj = np.zeros((6,6),dtype=complex)
    for i in range(6):
        proj += np.outer(evl[:,i],evr[:,i].conj())/np.dot(evr[:,i].conj(),evl[:,i])
    e_proj, evl_proj, evr_proj = eig(proj, left=True, right=True)
    print(e_proj)
    exit()

    fig, axs = plt.subplots(2, 2)
    axs[0, 0].imshow(np.abs(evr)) # TL
    axs[0, 0].set_title("vr")
    axs[0, 1].imshow(np.abs((evl))) # TR
    axs[0, 1].set_title("vl")
    axs[1, 0].imshow(np.abs(evr_proj)) # BL
    axs[1, 0].set_title("vr proj")
    axs[1, 1].imshow(np.abs(evl_proj)) # BR
    axs[1, 1].set_title("vl proj")
    plt.tight_layout()
    plt.show()
    
