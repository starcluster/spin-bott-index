import numpy as np
import ctypes
from numpy.ctypeslib import ndpointer
from os.path import exists
import os
from mpmath import polylog
from mpmath import exp
from mpmath import log
import lattice_f as lf
import lattice_r as lr
import matplotlib.pyplot as plt
import matplotlib
import tex
import time
import GS15 as gs15
import generate_mk as gMk
import shutil
import seaborn as sns
from scipy.optimize import curve_fit
from collections import deque
from sympy import *
from functools import partial

from numpy import linalg as LA
import tight_binding as tb
import chern as ch
import band_diagram as bd

tex.useTex()

A = 3 * np.sqrt(3) / 2


plasma_blue = (13 / 255, 8 / 255, 135 / 255)
plasma_purple = (139 / 255, 10 / 255, 165 / 255)
plasma_pink = (219 / 255, 92 / 255, 104 / 255)
plasma_orange = (254 / 255, 188 / 255, 43 / 255)

# gcc -c -Wextra -Wall -O2 6cell.c Faddeeva.c Li2.c Li3.c && gcc -shared -o 6cell.so generate_mk.o Faddeeva.o Li2.o Li3.o -lm

lib = ctypes.cdll.LoadLibrary("./6cell.so")
M = lib.M
M.restype = None
M.argtypes = [
    ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
    ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
    ctypes.c_int,
    ctypes.c_int,
    ctypes.c_int,
    ctypes.c_int,
    ctypes.c_double,
    ctypes.c_double,
    ctypes.c_double,
    ctypes.c_int,
    ctypes.c_double,
]

Mr = lib.Mr
Mr.restype = None
Mr.argtypes = [
    ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
    ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
    ctypes.c_int,
    ctypes.c_int,
    ctypes.c_int,
    ctypes.c_int,
    ctypes.c_double,
    ctypes.c_double,
    ctypes.c_double,
    ctypes.c_int,
    ctypes.c_double,
]


def gamma(d):
    return 3 * (
        (
            -log(1 + exp(1j * d)) / d
            + 1j * polylog(2, -exp(1j * d)) / d**2
            - polylog(3, -exp(1j * d)) / d**3
        )
    )


def generateMr_wrapper(k, distance, deltaB, deltaAB, k0, aho, N, a):
    Mk = np.zeros((12, 12), dtype=complex)
    d = 1 / np.sqrt(2) * np.array([[1, 1j], [-1, 1j]])
    for i in range(6):
        for j in range(6):
            Mreim = np.array([0] * 2, dtype="float64")
            Mr(k, Mreim, 0, 0, i, j, aho, k0, distance, N, a)
            Mk[2 * i, 2 * j] = Mreim[0] + 1j * Mreim[1]
            Mr(k, Mreim, 0, 1, i, j, aho, k0, distance, N, a)
            Mk[2 * i + 1, 2 * j] = Mreim[0] + 1j * Mreim[1]
            Mr(k, Mreim, 1, 0, i, j, aho, k0, distance, N, a)
            Mk[2 * i, 2 * j + 1] = Mreim[0] + 1j * Mreim[1]
            Mr(k, Mreim, 1, 1, i, j, aho, k0, distance, N, a)
            Mk[2 * i + 1, 2 * j + 1] = Mreim[0] + 1j * Mreim[1]
            Mkij = Mk[2 * i : 2 * i + 2, 2 * j : 2 * j + 2]
            Mk[2 * i : 2 * i + 2, 2 * j : 2 * j + 2] = Mkij
    Mk = 3 / 2 * Mk
    return Mk


def generate_mk_wrapper(k, distance, k0, aho, N, a):
    Mk = np.zeros((12, 12), dtype=complex)

    d = 1 / np.sqrt(2) * np.array([[1, 1j], [-1, 1j]])
    for i in range(6):
        for j in range(6):
            Mreim = np.array([0] * 2, dtype="float64")
            M(k, Mreim, 0, 0, i, j, aho, k0, distance, N, a)  # ou M(...
            Mk[2 * i, 2 * j] = Mreim[0] + 1j * Mreim[1]
            M(k, Mreim, 0, 1, i, j, aho, k0, distance, N, a)
            Mk[2 * i + 1, 2 * j] = Mreim[0] + 1j * Mreim[1]
            M(k, Mreim, 1, 0, i, j, aho, k0, distance, N, a)
            Mk[2 * i, 2 * j + 1] = Mreim[0] + 1j * Mreim[1]
            M(k, Mreim, 1, 1, i, j, aho, k0, distance, N, a)
            Mk[2 * i + 1, 2 * j + 1] = Mreim[0] + 1j * Mreim[1]
            Mkij = Mk[2 * i : 2 * i + 2, 2 * j : 2 * j + 2]
            Mkij = (d @ Mkij) @ (d.conj().T)
            Mk[2 * i : 2 * i + 2, 2 * j : 2 * j + 2] = Mkij

    Mk = -6 * np.pi / k0 * Mk
    if distance != 0:
        for i in range(12):
            Mk[i, i] += 1j + gMk.gamma(distance)

    return Mk


def band_diagram(lattice, distance, deltaB, deltaAB, k0, aho, N, a):
    X, Y, bands, corrs = [], [], [], []
    omegas, gammas = [], []

    for kxx, kyy in lattice:
        X.append(kxx)
        Y.append(kyy)
        k = np.array([kxx, kyy], dtype=float)

        Mk = generate_mk_wrapper(k, distance, k0, aho, N, a)
        lambdas, vects = np.linalg.eig(Mk)

        omega = [0] * 12
        gamma = [0] * 12
        k_norm = np.linalg.norm(k)

        vects = [vects[:, i] for i in range(12)]
        for j in range(12):
            gamma[j] = lambdas[j].imag
            omega[j] = -0.5 * lambdas[j].real

        try:
            tuples = sorted(zip(omega, vects, gamma))
        except ValueError:
            print(omega, vects, gamma)
        omega, vects, gamma = [[t[i] for t in tuples] for i in range(3)]

        corr, band = [], []
        for i in range(12):
            select_even = [2 * i for i in range(6)]
            u = np.choose(select_even, vects[i])
            corr.append(project_on_basis(u))
            band.append(omega[i])

        corrs.append(corr)
        bands.append(band)

        omegas += omega
        gammas += gamma
    B6 = [e[5] for e in bands]
    B7 = [e[6] for e in bands]
    print(lattice)
    return X, Y, omegas, gammas, np.min(B7), np.max(B6), bands, corrs


def plot_2cell(distance, deltaB, deltaAB, k0, aho, N, a):
    lattice = lf.generate_path_bz_folded(Ndots)
    B1, B2, B3, B4, X, Y, omegas, gammas, weightas, sigmas = bd.band_diagram(
        lattice, distance, deltaB, deltaAB, k0, aho, N, a
    )
    xprime = [i for i in range(len(omegas))]
    f = plt.figure()
    ax = f.add_subplot(111)
    im = ax.scatter(
        xprime,
        omegas,
        c=gammas,
        cmap="plasma",
        marker="o",
        s=5,
        norm=matplotlib.colors.Normalize(vmin=0.001, vmax=100),
        zorder=2,
    )
    ax.axhspan(min(B2), max(B3), alpha=0.5, color="gray")
    ax.set_ylabel(r"$\textrm{Frequency}$ $(\omega-\omega_0)/\Gamma_0$", fontsize=20)
    ax.set_xlabel(r"$\textrm{Quasimomentum}$ $k$", fontsize=20)
    ax.axis((0, len(xprime), -200, 150))
    cbar_ax = f.add_axes([0.91, 0.11, 0.05, 0.73])
    clb = f.colorbar(im, cax=cbar_ax)
    clb.ax.set_title(r"$\Gamma/\Gamma_0$")
    n = 4 * Ndots

    ax.axvline(n // 5, color="black", linestyle="--")
    # ax.axvline(n*2,color="black",linestyle="--")
    ax.axvline(2 * n // 5, color="black", linestyle="--")
    ax.axvline(3 * n // 5, color="black", linestyle="--")
    ax.set_xticks(
        [0, n // 5, 2 * n // 5, 3 * n // 5, 4 * n // 5, n],
        [
            "$\widetilde{K}$",
            "$\widetilde{\Sigma}$",
            "$\Gamma$",
            "$\widetilde{T}$",
            "$\widetilde{K}$",
            "$\widetilde{M}$",
        ],
        fontsize=20,
    )
    f.suptitle(
        r"$a=" + str(a) + ",d=" + str(distance) + ",\Delta_B=" + str(deltaB) + "$",
        fontsize=20,
    )
    plt.savefig(
        f"2cell_a{a}_d{distance}_dB{deltaB}.pdf", format="pdf", bbox_inches="tight"
    )
    plt.show()


def plot_6cell(distance, deltaB, deltaAB, k0, aho, N, a, Ndots):
    lattice = lf.generate_path_bz_6cell(a, Ndots)
    X, Y, omegas, gammas, bs, bi, bands, corrs = band_diagram(
        lattice, distance, deltaB, deltaAB, k0, aho, N, a
    )

    xprime = [i for i in range(len(omegas))]
    f = plt.figure()
    ax = f.add_subplot(111)
    im = ax.scatter(
        xprime,
        omegas,
        c=gammas,
        cmap="plasma",
        marker="o",
        s=5,
        norm=matplotlib.colors.Normalize(vmin=0.001, vmax=100),
        zorder=2,
    )
    ax.axhspan(bs, bi, alpha=0.5, color="gray")
    ax.set_ylabel(r"$\textrm{Frequency}$ $(\omega-\omega_0)/\Gamma_0$", fontsize=12)
    ax.set_xlabel(r"$\textrm{Quasimomentum}$ $k$", fontsize=12)
    ax.axis((0, len(xprime), -200, 150))
    # ax.axis((0,len(xprime),-2,2))
    cbar_ax = f.add_axes([0.91, 0.11, 0.05, 0.73])
    clb = f.colorbar(im, cax=cbar_ax)
    clb.ax.set_title(r"$\Gamma/\Gamma_0$")
    n = Ndots * 12
    ax.set_xticks([0, n // 2, n], ["$K$", "$\Gamma$", "$M$"], fontsize=20)
    f.suptitle(f"$a={np.round(a,2)},d={distance}$", fontsize=20)
    name = int(a * 10000)
    plt.savefig(f"6cell/{name}.png", format="png", bbox_inches="tight")
    plt.savefig(f"6cell_a{a}_d{distance}.pdf", format="pdf", bbox_inches="tight")
    plt.clf()
    plt.cla()
    return bs, bi


def gapa(distance, deltaB, deltaAB, k0, aho, N, a, Ndots, amin, amax, Na, rebake):
    """
    Computing and plotting width of the gap, inf boundary and sup
    boundary of the gap.
    """
    gaps = []
    ass = np.linspace(amin, amax, Na)
    borne_inf = []
    borne_sup = []
    gaps = []

    try:
        if rebake:
            raise OSError
        csv = np.genfromtxt(
            f"data/6cell_gap_d{distance}_amin{min(ass)}_amax{max(ass)}_Na{Na}.csv",
            delimiter=",",
        )
        gaps, borne_inf, borne_sup = tuple(csv.transpose().tolist())

    except OSError:
        for a in ass:
            bs, bi = plot_6cell(distance, deltaB, deltaAB, k0, aho, N, a, Ndots)
            borne_sup.append(bs)
            borne_inf.append(bi)
            gaps.append(bs - bi)
            print(f"a={a} bs={bs}")
            print(f"a={a} bi={bi}")

        np.savetxt(
            f"data/6cell_gap_d{distance}_amin{min(ass)}_amax{max(ass)}_Na{Na}.csv",
            [p for p in zip(gaps, borne_inf, borne_sup)],
            delimiter=",",
            fmt="%s",
        )

    # GAP
    plt.scatter(ass, gaps, color=plasma_pink, marker="^", s=70)
    plt.xlabel(r"$a$", fontsize=30)
    plt.ylabel(r"$W_{\textrm{gap}}$", fontsize=30)
    plt.xticks(fontsize=20, rotation=30)
    plt.yticks(fontsize=20)

    plt.savefig(
        f"6cell_gap_d{distance}_amin{min(ass)}_amax{max(ass)}_Na{Na}.png",
        format="png",
        bbox_inches="tight",
    )
    plt.savefig(
        f"6cell_gap_d{distance}_amin{min(ass)}_amax{max(ass)}_Na{Na}.pdf",
        format="pdf",
        bbox_inches="tight",
    )
    plt.clf()
    plt.cla()

    # BOUNDARIES
    plt.scatter(ass, borne_inf, color=plasma_pink, marker="^", s=70)
    plt.scatter(ass, borne_sup, color=plasma_blue, marker="v", s=70)
    plt.xlabel(r"$a$", fontsize=30)
    plt.ylabel(r"$\textrm{band}$", fontsize=30)
    plt.xticks(fontsize=20, rotation=30)
    plt.yticks(fontsize=20)

    plt.savefig(
        f"6cell_boundaries_d{distance}_amin{min(ass)}_amax{max(ass)}_Na{Na}.png",
        format="png",
        bbox_inches="tight",
    )
    plt.savefig(
        f"6cell_boundaries_d{distance}_amin{min(ass)}_amax{max(ass)}_Na{Na}.pdf",
        format="pdf",
        bbox_inches="tight",
    )


def color_s(s):
    if s < 0:
        return "red"
    else:
        return "blue"


def visualize_psi(vects, name_f):
    # basis_name_ltx = ["$s$","$p_x$","$p_y$","$d_{x^2y^2}$","$d_{xy}$","$f$"]
    basis_name_ltx = ["$s$", "$p_+$", "$p_-$", "$d_+$", "$d_-$", "$f$"]
    a = 1
    x_hex = [a, a / 2, -a / 2, -a, -a / 2, a / 2]
    y_hex = [
        0,
        a * np.sqrt(3) / 2,
        a * np.sqrt(3) / 2,
        0,
        -a * np.sqrt(3) / 2,
        -a * np.sqrt(3) / 2,
    ]

    if len(vects[0]) == 6:
        for i in range(len(vects)):
            vects[i] = np.repeat(vects[i], 2)

    select_even = [2 * i for i in range(6)]
    select_odd = [2 * i + 1 for i in range(6)]

    fig_re, axes_re = plt.subplots(6, 2)
    fig_norm, axes_norm = plt.subplots(6, 2)
    fig_norm_diff, axes_norm_diff = plt.subplots(6, 2)
    no_band = 1
    for vect, ax_re, ax_norm, ax_norm_diff in zip(
        vects, axes_re.flat, axes_norm.flat, axes_norm_diff.flat
    ):
        Re_psi = vect.real
        projected = project_on_basis(np.choose(select_even, vect))
        yl = [1.4 - i * 0.6 for i in range(6)]
        ax_re.text(s="$" + str(no_band) + "$", x=-0.2, y=0)
        no_band += 1
        for p, yc, name_ltx in zip(projected, yl, basis_name_ltx):
            ax_re.text(
                s=name_ltx + " $=" + str(np.round(p**2, 3)) + "$",
                x=-3.8,
                y=yc,
                fontsize="x-small",
            )
        for x, y, sp, sm in zip(
            x_hex, y_hex, np.choose(select_even, Re_psi), np.choose(select_odd, Re_psi)
        ):
            s1 = int(sp * 100)
            s2 = int(sm * 100)
            ax_re.set_xticks([])
            ax_re.set_yticks([])
            ax_re.scatter(x, y, s=abs(s1), color=color_s(s1))
            ax_re.scatter(x + 3, y, s=abs(s2), color=color_s(s2))

            ax_norm.scatter(
                x, y, s=int((np.abs(sp) ** 2 + np.abs(sm) ** 2) * 1000), color="black"
            )
            ax_norm_diff.scatter(
                x,
                y,
                s=abs(int((np.abs(sp) ** 2 - np.abs(sm) ** 2) * 1000)),
                color="black",
            )
            ax_re.axis((-4, 5, -2, 2))
            ax_norm.axis((-4, 2, -2, 2))
            ax_norm_diff.axis((-4, 2, -2, 2))

    fig_re.suptitle(
        r"$\textrm{Re}(\psi_{2i}(\Gamma))\quad\textrm{Re}(\psi_{2i+1}(\Gamma))\quad a="
        + str(np.round(a, 4))
        + "$",
        fontsize=20,
    )
    fig_re.savefig(name_f + ".png", bbox_inches="tight")
    fig_re.savefig(name_f + ".pdf", bbox_inches="tight")
    # fig_norm.suptitle(r"$|\psi_{2i}(\Gamma)|^2+|\psi_{2i+1}(\Gamma)|^2\quad a=" + str(np.round(a,4)) + "$")
    # fig_norm.savefig(name_f + ".png",bbox_inches='tight')
    # fig_norm_diff.suptitle(r"$|\psi_{2i}(\Gamma)|^2-|\psi_{2i+1}(\Gamma)|^2\quad a=" + str(np.round(a,4)) + "$")
    # fig_norm_diff.savefig(name_f +".png",bbox_inches='tight')

    print(a)
    plt.clf()
    plt.cla()


def histogramme(distance, deltaB, deltaAB, k0, aho, N, a, Ndots):
    lattice = lf.generate_bz_6cell(a, Ndots)
    l1, l2 = lf.generate_bz_6cell(a, 0.05)  # /!!!!!!\
    # plt.figure(figsize=(6,6))
    # gl.display_lattice(l1,"red")
    # gl.display_lattice(l2,"blue")
    # plt.axis((-2,2,-2,2))
    lattice = np.array(l1.tolist() + l2.tolist())  # + (0.001,0.001)
    # gl.display_lattice(lattice,"green")
    # plt.savefig("probleme_hist.pdf")
    # plt.show()
    # lattice  = gl.generate_bz_6cell(a,Ndots)

    X, Y, omegas, gammas, B6, B7, bs, bi, bands, corrs = band_diagram(
        lattice, distance, deltaB, deltaAB, k0, aho, N, a
    )
    # plot_6cell(distance, deltaB, deltaAB, k0, aho, N, a, Ndots)
    # print(omegas)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.hist(omegas, bins=100, color="purple")
    plt.title(f"$a={a}$", fontsize=20)
    # sns.set_style('whitegrid')
    # sns.kdeplot(np.array(omegas), bw_method=0.05)

    plt.savefig(f"hist_6cell_a{a}.pdf")
    plt.show()


def spdf_2():
    v_s = np.array([1, 1, 1, 1, 1, 1],dtype=complex)
    v_px = np.array([1, 1, 0, -1, -1, 0],dtype=complex)
    v_py = np.array([1, -1, -2, -1, 1, 2],dtype=complex)
    v_dx2y2 = np.array([1, 1, -2, 1, 1, -2],dtype=complex)
    v_dxy = np.array([1, -1, 0, 1, -1, 0],dtype=complex)
    v_f = np.array([1, -1, 1, -1, 1, -1],dtype=complex)

    v_s /= np.linalg.norm(v_s)
    v_px /= np.linalg.norm(v_px)
    v_py /=  np.linalg.norm(v_py)
    v_dx2y2 /=  np.linalg.norm(v_dx2y2)
    v_dxy /=np.linalg.norm(v_dxy)
    v_f /=  np.linalg.norm(v_f)

    v_pp = 1 / np.sqrt(2) * (v_px + 1j * v_py)
    v_pm = 1 / np.sqrt(2) * (v_px - 1j * v_py)

    v_dp = 1 / np.sqrt(2) * (v_dx2y2 + 1j * v_dxy)
    v_dm = 1 / np.sqrt(2) * (v_dx2y2 - 1j * v_dxy)

    # print(f"{np.linalg.norm(v_s)=}")
    # print(f"{np.linalg.norm(v_pp)=}")
    # print(f"{np.linalg.norm(v_pm)=}")
    # print(f"{np.linalg.norm(v_dp)=}")
    # print(f"{np.linalg.norm(v_dm)=}")
    # print(f"{np.linalg.norm(v_f)=}")
    # exit()
    basis = [
        v_s,
        v_pp, 
        v_pm,
        v_dp,
        v_dm,
        v_f 
    ]

    return basis


def spdf():
    v_s = np.array([1, 1, 1, 1, 1, 1])
    v_px = np.array([1, 1, 0, -1, -1, 0])
    v_py = np.array([1, -1, -2, -1, 1, 2])
    v_dx2y2 = np.array([1, 1, -2, 1, 1, -2])
    v_dxy = np.array([1, -1, 0, 1, -1, 0])
    v_f = np.array([1, -1, 1, -1, 1, -1])

    v_pp = 1 / np.sqrt(2) * (v_px + 1j * v_py)
    v_pm = 1 / np.sqrt(2) * (v_px - 1j * v_py)

    v_dp = 1 / np.sqrt(2) * (v_dx2y2 + 1j * v_dxy)
    v_dm = 1 / np.sqrt(2) * (v_dx2y2 - 1j * v_dxy)

    basis = [
        v_s / np.linalg.norm(v_s),
        v_pp / np.linalg.norm(v_pp),
        v_pm / np.linalg.norm(v_pm),
        v_dp / np.linalg.norm(v_dp),
        v_dm / np.linalg.norm(v_dm),
        v_f / np.linalg.norm(v_f),
    ]

    return basis


def spdf_sympy():
    v_s = Matrix([1, 1, 1, 1, 1, 1])
    v_px = Matrix([1, 1, 0, -1, -1, 0])
    v_py = Matrix([1, -1, -2, -1, 1, 2])
    v_dx2y2 = Matrix([1, 1, -2, 1, 1, -2])
    v_dxy = Matrix([1, -1, 0, 1, -1, 0])
    v_f = Matrix([1, -1, 1, -1, 1, -1])

    v_s /= S(v_s.norm())

    v_px /= S(v_px.norm())
    v_py /= S(v_py.norm())
    v_dx2y2 /= S(v_dx2y2.norm())
    v_dxy /= S(v_dxy.norm())
    v_f /= S(v_f.norm())

    v_pp = S(1 / sqrt(2)) * (v_px + I * v_py)
    v_pm = 1 / sqrt(2) * (v_px - I * v_py)

    v_dp = S(1 / sqrt(2)) * (v_dx2y2 + I * v_dxy)
    v_dm = 1 / sqrt(2) * (v_dx2y2 - I * v_dxy)

    return Matrix.hstack(v_pp, v_dp, v_pm, v_dm, v_s, v_f)


def project_on_basis(vect):
    basis = spdf()

    basis_name = ["s", "px", "py", "dx2y2", "dxy", "f"]

    projection = []
    for b, bn in zip(basis, basis_name):
        p = 0
        b2 = deque(b)
        for _ in range(6):
            p2 = np.linalg.norm(np.dot(np.conj(vect) / np.linalg.norm(vect), b2))
            if p2 > p:
                p = p2
            b2.rotate(1)
        projection.append(p)

    return projection


def plot_6cell_basis(distance, deltaB, deltaAB, k0, aho, N, a, Ndots):
    lattice = lf.generate_path_bz_6cell(a, Ndots)

    X, Y, omegas, gammas, bs, bi, bands, corrs = band_diagram(
        lattice, distance, deltaB, deltaAB, k0, aho, N, a
    )

    B6 = [e[5] for e in bands]

    color_bands = []
    for corr in corrs:
        cbs = []
        for e in corr:
            pp = e[1]  # p+
            pm = e[2]  # p-
            dp = e[3]  # d+
            dm = e[4]  # d-

            cb = max(LA.norm(pp), LA.norm(pm)) - max(LA.norm(dp), LA.norm(dm))
            cbs.append(cb)
        color_bands.append(cbs)

    xprime = [i for i in range(len(B6))]
    f = plt.figure()
    ax = f.add_subplot(111)
    for i in range(12):
        im = ax.scatter(
            xprime,
            [b[i] for b in bands],
            c=[c[i] for c in color_bands],
            cmap="jet",
            marker="o",
            s=10,
            zorder=2,
            norm=matplotlib.colors.Normalize(vmin=-1, vmax=1),
        )

    ax.axhspan(bs, bi, alpha=0.5, color="gray")
    ax.set_ylabel(r"$\textrm{Frequency}$ $(\omega-\omega_0)/\Gamma_0$", fontsize=20)
    ax.set_xlabel(r"$\textrm{Quasimomentum}$ $k$", fontsize=20)
    # ax.axis((0,len(xprime),-200,150))
    ax.axis((0, len(xprime), -20, 20))
    cbar_ax = f.add_axes([0.91, 0.11, 0.05, 0.73])
    clb = f.colorbar(im, cax=cbar_ax)
    clb.ax.set_title(r"$<,>$")

    n = Ndots // 2
    # ax.set_xticks([0,n,2*n],["$K$","$\Gamma$","$M$"],fontsize=20)
    ax.set_xticks([n], ["$\Gamma$"], fontsize=20)
    f.suptitle(f"$a={np.round(a,3)},d={distance}$", fontsize=20)
    name = int(a * 10000)

    plt.savefig(f"zoom/{int(a*1000)}.png", format="png", bbox_inches="tight")

    plt.clf()
    plt.cla()
    return bs, bi


def get_H(k, distance, k0, aho, N, a):
    M0 = generate_mk_wrapper(k, distance, k0, aho, N, a)
    return tb.psp_from_H(M0)


def spin_chern(distance, deltaB, deltaAB, k0, aho, N, a):
    k = np.array([0.0, 0.0], dtype=float)

    lattice, _, Dk = lf.generate_bz_6cell(a=1, step=0.1)
    dim = 12
    C = ch.chern(
        partial(get_H, distance=distance, k0=k0, aho=aho, N=N, a=a), lattice, Dk, dim
    )
    print(C)


if __name__ == "__main__":
    distance = 0
    deltaB = 0
    deltaAB = 0
    k0 = 2 * np.pi * 0.05
    # k0 = 1.55
    a = 0.95
    aho = 0.1
    N = 80

    Ndots = 500

    spin_chern(distance, deltaB, deltaAB, k0, aho, N, a)
    exit()
    # plot_2cell(distance, deltaB, deltaAB, k0, aho, N, a)
    # exit()
    # plot_6cell(distance, deltaB, deltaAB, k0, aho, N, a, Ndots)
    # exit()

    print("Making Mks empty...")
    shutil.rmtree("Mks")
    os.mkdir("Mks")

    amin = 0.9
    amax = 1.1
    Na = 20
    rebake = True

    Gamma = np.array([0, 0], dtype="float64")

    Mk = generate_mk_wrapper(Gamma, distance, k0, aho, N, a)
    lambdas, vects = np.linalg.eig(Mk)

    omega = [0] * 12
    gamma = [0] * 12

    vects = [vects[:, i] for i in range(12)]
    for j in range(12):
        gamma[j] = lambdas[j].imag
        omega[j] = -0.5 * lambdas[j].real

    tuples = sorted(zip(omega, vects, gamma))
    omega, vects, gamma = [[t[i] for t in tuples] for i in range(3)]

    name_f = f"visu_psi/re/{int(a*1000)}"
    # visualize_psi(vects,name_f)
    # exit()
    # Mk = generate_mk_wrapper(Gamma, distance, deltaB, deltaAB, k0, aho, N, a)
    # lambdas, vects = np.linalg.eig(Mk)

    # omega = [0]*12
    # gamma = [0]*12
    # k_norm = np.linalg.norm(k)

    # vects = [vects[:,i] for i in range(12)]
    # for j in range(12):
    #     gamma[j] = lambdas[j].imag
    #     omega[j] = -0.5*lambdas[j].real

    # tuples = sorted(zip(omega, vects, gamma))
    # omega, vects, gamma = [[t[i] for t in tuples] for i in range(3)]

    # corr,band = [],[]
    # for i in range(12):
    #     corr.append(project_on_basis(vects[i]))
    #     band.append(omega[i])

    # corrs.append(corr)
    # bands.append(band)

    # plot_6cell(distance, deltaB, deltaAB, k0, aho, N, a, Ndots)

    # l1,l2 = gl.generate_bz(a, 900, 0.1)
    # gl.display_lattice(l1,"red")
    # gl.display_lattice(l2,"blue")

    # plt.axis((-3,3,-3,3))
    # plt.savefig("2.pdf")
    # plt.show()
    # histogramme(distance, deltaB, deltaAB, k0, aho, N, 0.92, Ndots)
    # histogramme(distance, deltaB, deltaAB, k0, aho, N, 1.1, Ndots)
    # histogramme(distance, deltaB, deltaAB, k0, aho, N, 1., Ndots)
    # exit()

    for a in [1 + 0.001 * i for i in range(70)]:
        plot_6cell_basis(distance, deltaB, deltaAB, k0, aho, N, a, Ndots)
    exit()

    # lattice  = gl.generate_bz_6cell(a,Ndots)
    # for Gamma in lattice:
    #     project_on_basis(distance, deltaB, deltaAB, k0, aho, N, a, Gamma, rebake)
    # exit()

    # exit()
    # gapa(distance, deltaB, deltaAB, k0, aho, N, a, Ndots, amin, amax, Na, rebake)

    for a in [0.9 + i / 100 for i in range(20)]:
        visualize_psi(distance, deltaB, deltaAB, k0, aho, N, a, Gamma, rebake)
